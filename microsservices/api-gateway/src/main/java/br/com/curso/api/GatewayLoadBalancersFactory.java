package br.com.curso.api;

import io.micronaut.context.annotation.Factory;
import io.micronaut.http.client.LoadBalancer;
import io.micronaut.http.client.loadbalance.DiscoveryClientLoadBalancerFactory;
import jakarta.inject.Singleton;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Factory
public class GatewayLoadBalancersFactory {

    @Singleton
    public Map<String, LoadBalancer> serviceLoadBalancers(GatewayProperties gatewayProperties, DiscoveryClientLoadBalancerFactory factory) {
        var services = gatewayProperties.getServices();
        var loadBalancers = new HashMap<String, LoadBalancer>();
        services.forEach(serviceName -> loadBalancers.put(serviceName, factory.create(serviceName)));

        return Collections.unmodifiableMap(loadBalancers);
    }
}
